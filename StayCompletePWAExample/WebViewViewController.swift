//
//  WebViewViewController.swift
//  StayFrontpageAPIExample
//
//  Created by Alberto Moraga on 21/01/2020.
//  Copyright © 2020 Stay. All rights reserved.
//

import UIKit
import WebKit
import StayDigitalKeyFramework

class WebViewViewController: UIViewController, WKScriptMessageHandler, WKNavigationDelegate {
    public var urlString: String = ""
    var digitalKeyModule: DigitalKeyModule!

    @IBOutlet weak var webView: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

      
        
        self.setupWebView();
        self.view.addSubview(self.webView)
      //  webView.navigationDelegate = self
        
/* Uncomment to show JS logs
        let source = "function captureLog(msg) { window.webkit.messageHandlers.logHandler.postMessage(msg); } window.console.log = captureLog;"
        let script = WKUserScript(source: source, injectionTime: .atDocumentEnd, forMainFrameOnly: false)
        webView.configuration.userContentController.addUserScript(script)
        webView.configuration.userContentController.add(self, name: "logHandler")
*/

        //loadPWA()
        
        PassKitHelper.suppressApplePay();
        
        
      
        if let url = URL(string: "https://pwa-int.stay-app.com/?id=pJ0q&utm_campaign=anonymous&utm_medium=email&utm_source=checkin&token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODcxNzY5ODAsImRhdGEiOnsiYXBwSWQiOjEwOSwiYXBpS2V5IjoiYjQzMTQ1ODIyNGJmYTNiZTQwODJmMDU3MWJmOGEzZTEifSwiZXhwIjoxNjg3MjYzMzgwfQ.PDbY_34VojfI-ypcbsVoeWg83egMg6vUb1G6_i-WesU") { //QqYY //QbkJ
            let request = URLRequest(url: url)
            self.webView.load(request)
            // webView.scrollView.contentInsetAdjustmentBehavior = .never;
        }
    }
    
    
    private func setupWebView() {
        
        let contentController = WKUserContentController()
        let userScript = WKUserScript(
            source: "mobileHeader()",
            injectionTime: .atDocumentEnd,
            forMainFrameOnly: true
        )
        contentController.addUserScript(userScript)
        contentController.add(self, name: "DigitalKey")
        contentController.add(self, name: "log")
        let config = WKWebViewConfiguration()
        config.userContentController = contentController
        self.webView = WKWebView(frame: self.view.bounds, configuration: config)
        
        digitalKeyModule = DigitalKeyModule()
        digitalKeyModule.webview = self.webView;

        
    }
    
    public func loadPWA() {
        let url = URL(string: urlString)!
        //let url = URL(string: "http://192.168.2.230:8080/?id=K9aN&utm_campaign=anonymous&utm_medium=email&utm_source=checkin")! //test local
        webView.load(URLRequest(url: url))
    }
    
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }

    
}

extension WebViewViewController {
    static var startTime = 0.0

    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        WebViewViewController.startTime = CFAbsoluteTimeGetCurrent()
    }

    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
    }
    
    @available(iOS 13.0, *)
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, preferences: WKWebpagePreferences, decisionHandler: @escaping (WKNavigationActionPolicy, WKWebpagePreferences) -> Void) {
        if let url = navigationAction.request.url, url.absoluteString.contains("exit-establishment") {
            dismiss(animated: true, completion: nil)
        }
        decisionHandler(.allow, preferences)
    }
    
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        
        if message.name == "DigitalKey" {
            if let bodyString = message.body as? String {
                digitalKeyModule.didReceiveMessage(bodyString)
            }
        } else if message.name == "logHandler" {
            let time = CFAbsoluteTimeGetCurrent() - WebViewViewController.startTime
            print("LOG \(time): \(message.body)")
        }

    }
}
