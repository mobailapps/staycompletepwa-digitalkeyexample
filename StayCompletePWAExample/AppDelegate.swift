//
//  AppDelegate.swift
//  StayFrontpageAPIExample
//
//  Created by Alberto Moraga on 13/01/2020.
//  Copyright © 2020 Stay. All rights reserved.
//

import UIKit
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        UNUserNotificationCenter.current().delegate = self
        registerForPushNotifications()

        if let notification = launchOptions?[UIApplication.LaunchOptionsKey.remoteNotification] as? [String:AnyObject] {
            openNotification(notification: notification)
        }
        return true
    }

    // MARK: UISceneSession Lifecycle

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
    }

    func registerForPushNotifications() {
      UNUserNotificationCenter.current()
        .requestAuthorization(options: [.alert, .sound, .badge]) {
          [weak self] granted, error in
            
          print("Permission granted: \(granted)")
          guard granted else { return }
          self?.getNotificationSettings()
      }
    }
    
    func getNotificationSettings() {
      UNUserNotificationCenter.current().getNotificationSettings { settings in
        guard settings.authorizationStatus == .authorized else { return }
        DispatchQueue.main.async {
          UIApplication.shared.registerForRemoteNotifications()
        }
      }
    }

    func application(
      _ application: UIApplication,
      didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data
    ) {
      let token = deviceToken.map { String(format: "%02x", $0) }.joined()
      print("Device Token: \(token)")
      UserDefaults.standard.set(token, forKey: "push_token")
      let window = UIApplication.shared.windows.filter {$0.isKeyWindow}.first
      let navigationViewController = window?.rootViewController as? UINavigationController
      let viewController = navigationViewController?.topViewController as? FormViewController
      viewController?.setupAdditionalData()
    }

    func application(
      _ application: UIApplication,
      didFailToRegisterForRemoteNotificationsWithError error: Error) {
      print("Failed to register: \(error)")
    }
    
    func openNotification(notification: [String:AnyObject]) {
        if let extra = notification["extra"] as? [String:AnyObject] {
            if let url = extra["url"] as? String {
                showWebView(urlString: url)
            }
        }
    }

    func showWebView(urlString: String) {
        let webViewViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "WebViewViewController") as? WebViewViewController
        if let webViewViewController = webViewViewController {
            webViewViewController.urlString = urlString
            let window = UIApplication.shared.windows.filter {$0.isKeyWindow}.first
            let navigationViewController = window?.rootViewController as? UINavigationController
            navigationViewController?.pushViewController(webViewViewController, animated: true)
        }
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
           willPresent notification: UNNotification,
           withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler(.alert)
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        if let notification = response.notification.request.content.userInfo as? [String : AnyObject] {
            openNotification(notification: notification)
        }
        completionHandler()
    }
}

