//
//  ViewController.swift
//  StayFrontpageAPIExample
//
//  Created by Alberto Moraga on 13/01/2020.
//  Copyright © 2020 Stay. All rights reserved.
//

import UIKit

class FormViewController: UIViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var prodEnvironmentSwitch: UISwitch!
    @IBOutlet weak var locatorTextField: UITextField!
    @IBOutlet weak var languageTextField: UITextField!
    @IBOutlet weak var establishmentIdTextField: UITextField!
    @IBOutlet weak var roomTextField: UITextField!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var checkInTextField: UITextField!
    @IBOutlet weak var checkOutTextField: UITextField!
    @IBOutlet weak var eventTypeTextField: UITextField!
    @IBOutlet weak var sendButton: UIBarButtonItem!
    @IBOutlet weak var deviceUidLabel: UILabel!
    @IBOutlet weak var pushTokenLabel: UILabel!
    @IBOutlet weak var countryLabel: UILabel!
    @IBOutlet weak var osLabel: UILabel!
    @IBOutlet weak var versionLabel: UILabel!
    @IBOutlet weak var modelLabel: UILabel!
    @IBOutlet weak var externalProviderIdLabel: UILabel!
    @IBOutlet weak var tokenLabel: UILabel!
    @IBOutlet weak var pickerViewDoneButton: UIButton!
    @IBOutlet weak var deleteTokenButton: UIButton!
    @IBOutlet weak var copyPushTokenButton: UIButton!
    
    var activeTextField = UITextField()
    var spinnerView : UIView?

    let checkinDatePicker = UIDatePicker()
    let checkoutDatePicker = UIDatePicker()
    
    final let clientSecretCluster = "030c7772-eccc-46fa-b1ae-e7fb1221a82c"
    final let clientIdCluster = "stayExternal"
    final let grantTypeCluster = "password"
    final let passwordCluster = "st4y3xterNal"
    final let usernameCluster = "stayexternal"
    final let provider = "3" // Stay
    final let applicationIdentifier = "60781a9d7540bf0018614bf8" // Stay 113
//    final let provider = "20" // Riu
//    final let applicationIdentifier = "5f0eb8ef2f82f7003001ea85" // Riu
    
    final let defaultProdEnvironment = false

    var bearer = String()
    var prodEnvironment = Bool()

    var dataObject: Dictionary<String, Any> = [:]
    var establishmentsList = Array<Any>()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupDatePicker(datePicker: checkinDatePicker, textField: checkInTextField)
        setupDatePicker(datePicker: checkoutDatePicker, textField: checkOutTextField)
        setupEstablishmentIdTextField()
                
        loadData()
        setupAdditionalData()

        if validForm() {
            getAppBearer{
                self.login()
            }
        }
        
        let tapGesture = UITapGestureRecognizer(target: view, action: #selector(UIView.endEditing))
        view.addGestureRecognizer(tapGesture)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(textFieldDidChange), name: UITextField.textDidChangeNotification, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: UITextField.textDidChangeNotification, object: nil)
    }
    
    func setupEstablishmentIdTextField() {
        let label = UILabel()
        label.text = "+ "
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.selectEstablishmentFromList))
        tap.numberOfTapsRequired = 1
        label.isUserInteractionEnabled = true
        label.addGestureRecognizer(tap)
        establishmentIdTextField.rightView = label
        establishmentIdTextField.rightViewMode = .always
    }
    
    func loadData() {
        prodEnvironment = (UserDefaults.standard.string(forKey: "prodEnvironment") != nil) ? UserDefaults.standard.bool(forKey: "prodEnvironment") : defaultProdEnvironment
        prodEnvironmentSwitch.setOn(prodEnvironment, animated: false)
        locatorTextField.text = UserDefaults.standard.string(forKey: "locator") ?? nil
        languageTextField.text = UserDefaults.standard.string(forKey: "language") ?? nil
        establishmentIdTextField.text = UserDefaults.standard.string(forKey: "establishmentId") ?? nil
        roomTextField.text = UserDefaults.standard.string(forKey: "room") ?? nil
        nameTextField.text = UserDefaults.standard.string(forKey: "name") ?? nil
        checkInTextField.text = UserDefaults.standard.string(forKey: "checkin") ?? nil
        checkOutTextField.text = UserDefaults.standard.string(forKey: "checkout") ?? nil
        eventTypeTextField.text = UserDefaults.standard.string(forKey: "eventType") ?? nil
        sendButton.isEnabled = validForm()
    }

    func setupAdditionalData() {
        let uniqueIdentifier = UIDevice.current.identifierForVendor
        deviceUidLabel.text = uniqueIdentifier?.uuidString
        countryLabel.text = (NSLocale.current.regionCode ?? "GB")
        osLabel.text = UIDevice.current.systemName.lowercased()
        versionLabel.text = UIDevice.current.systemVersion
        modelLabel.text = UIDevice.current.model
        tokenLabel.text = UserDefaults.standard.string(forKey: "client_bearer_token") ?? "Empty"
        deleteTokenButton.isHidden = tokenLabel.text == "Empty"
        pushTokenLabel.text = UserDefaults.standard.string(forKey: "push_token") ?? "Empty"
        copyPushTokenButton.isHidden = pushTokenLabel.text == "Empty"
        externalProviderIdLabel.text = provider
    }
    
    func setupDatePicker(datePicker: UIDatePicker, textField: UITextField) {
        datePicker.datePickerMode = .date
        if #available(iOS 13.4, *) {
            datePicker.preferredDatePickerStyle = .wheels
        }
        textField.inputView = datePicker
        textField.inputAccessoryView = pickerViewDoneButton
        pickerViewDoneButton.removeFromSuperview()
        datePicker.addTarget(self, action: #selector(handleDatePicker(sender:)), for: .valueChanged)
    }
    
    @objc func handleDatePicker(sender: UIDatePicker) {
        let textField = sender == checkinDatePicker ? checkInTextField : checkOutTextField;
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        textField?.text = dateFormatter.string(from: sender.date)
    }
    
    func saveData() {
        UserDefaults.standard.set(prodEnvironment, forKey: "prodEnvironment")
        if let locator = locatorTextField.text, !locator.isEmpty {
            UserDefaults.standard.set(locator, forKey: "locator")
        } else {
            UserDefaults.standard.removeObject(forKey: "locator")
        }
        if let language = languageTextField.text, !language.isEmpty {
            UserDefaults.standard.set(language, forKey: "language")
        } else {
            UserDefaults.standard.removeObject(forKey: "language")
        }
        if let establishmentId = establishmentIdTextField.text, !establishmentId.isEmpty {
            UserDefaults.standard.set(establishmentId, forKey: "establishmentId")
        } else {
            UserDefaults.standard.removeObject(forKey: "establishmentId")
        }
        if let completeName = nameTextField.text, !completeName.isEmpty {
            UserDefaults.standard.set(completeName, forKey: "name")
        } else {
            UserDefaults.standard.removeObject(forKey: "name")
        }
        if let room = roomTextField.text, !room.isEmpty {
            UserDefaults.standard.set(room, forKey: "room")
        } else {
            UserDefaults.standard.removeObject(forKey: "room")
        }
        if let checkIn = checkInTextField.text, !checkIn.isEmpty {
            UserDefaults.standard.set(checkIn, forKey: "checkin")
        } else {
            UserDefaults.standard.removeObject(forKey: "checkin")
        }
        if let checkOut = checkOutTextField.text, !checkOut.isEmpty {
            UserDefaults.standard.set(checkOut, forKey: "checkout")
        } else {
            UserDefaults.standard.removeObject(forKey: "checkout")
        }
        if let eventType = eventTypeTextField.text, !eventType.isEmpty {
            UserDefaults.standard.set(eventType, forKey: "eventType")
        } else {
            UserDefaults.standard.removeObject(forKey: "eventType")
        }
    }

    func resetData() {
        UserDefaults.standard.removeObject(forKey: "client_bearer_token")
        UserDefaults.standard.removeObject(forKey: "locator")
        UserDefaults.standard.removeObject(forKey: "language")
        UserDefaults.standard.removeObject(forKey: "establishmentId")
        UserDefaults.standard.removeObject(forKey: "room")
        UserDefaults.standard.removeObject(forKey: "name")
        UserDefaults.standard.removeObject(forKey: "checkin")
        UserDefaults.standard.removeObject(forKey: "checkout")
        UserDefaults.standard.removeObject(forKey: "eventType")
        UserDefaults.standard.removeObject(forKey: "prodEnvironment")
        prodEnvironment = defaultProdEnvironment
        prodEnvironmentSwitch.setOn(prodEnvironment, animated: false)
        locatorTextField.text = nil
        languageTextField.text = nil
        establishmentIdTextField.text = nil
        roomTextField.text = nil
        nameTextField.text = nil
        checkInTextField.text = nil
        checkOutTextField.text = nil
        eventTypeTextField.text = nil
        setupAdditionalData()
        sendButton.isEnabled = validForm()
    }
    
    func showEstablishmentsList() {
        DispatchQueue.main.async {
            let establishmentsListViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "EstablishmentsListTableViewController") as! EstablishmentsListTableViewController
            establishmentsListViewController.delegate = self
            establishmentsListViewController.establishments = self.establishmentsList
            establishmentsListViewController.establishments = self.establishmentsList.filter { establishment in
                return (establishment as? [String:Any])?["externalId"] != nil
            }
            self.present(establishmentsListViewController, animated: true, completion: nil)
        }
    }
    
    func validForm() -> Bool {
        return !(establishmentIdTextField.text?.isEmpty ?? false)
    }
    
    func selectNextTextField() {
        switch activeTextField {
        case locatorTextField:
            languageTextField.becomeFirstResponder()
        case languageTextField:
            establishmentIdTextField.becomeFirstResponder()
        case establishmentIdTextField:
            roomTextField.becomeFirstResponder()
        case roomTextField:
            nameTextField.becomeFirstResponder()
        case nameTextField:
            checkInTextField.becomeFirstResponder()
        case checkInTextField:
            checkOutTextField.becomeFirstResponder()
        case checkOutTextField:
            eventTypeTextField.becomeFirstResponder()
        case eventTypeTextField:
            locatorTextField.becomeFirstResponder()
        default:
            locatorTextField.resignFirstResponder()
        }
    }
    
    @objc func selectEstablishmentFromList() {
        if (establishmentsList.isEmpty) {
            getAppBearer{
                self.getEstablishments()
            }
        } else {
            showEstablishmentsList()
        }
    }
    
    func getAppBearer(completion: @escaping () -> Void) {
        showSpinner(onView: self.view)
        var url = URL(string: "https://auth.stay-app.com/auth/realms/api_users/protocol/openid-connect/token")!
        if (prodEnvironment) {
            url = URL(string: "https://auth.stay-app.com/auth/realms/api_users/protocol/openid-connect/token")!
        }
        
        let bodyString : Data = "client_secret=\(clientSecretCluster)&client_id=\(clientIdCluster)&grant_type=\(grantTypeCluster)&password=\(passwordCluster)&username=\(usernameCluster)".data(using: .utf8)!
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = bodyString
        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            if let error = error {
                print("error: \(error)")
                self.removeSpinner()
                self.showError(error.localizedDescription)
            } else {
                if let response = response as? HTTPURLResponse {
                    print("getAppBearer statusCode: \(response.statusCode)")
                }
                if let data = data, let dataString = String(data: data, encoding: .utf8) {
                    print("getAppBearer data: \(dataString)")
                    do {
                        let dataObject = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.allowFragments) as Any
                        if let dataObject = dataObject as? [String: Any], let access_token = dataObject["access_token"] {
                            UserDefaults.standard.set(access_token, forKey: "bearer_token")
                            completion()
                        } else{
                            self.removeSpinner()
                            if let dataObject = dataObject as? [String: Any], let error = dataObject["message"] as? String {
                                self.showError(error)
                            }
                        }
                    } catch {
                        self.removeSpinner()
                        self.showError("parse error")
                    }
                } else {
                    self.removeSpinner()
                    self.showError("parse error")
                }
            }
        }
        task.resume()
    }
    
    func getEstablishments() {
        showSpinner(onView: self.view)
        var url = URL(string: "https://cluster-dev.stay-app.com/int/establishment/application/"+applicationIdentifier+"/provider/"+provider)!
        if (prodEnvironment) {
            url = URL(string: "https://cluster.stay-app.com/establishment/application/"+applicationIdentifier+"/provider/"+provider)!
        }
        
        guard let bearer_token = UserDefaults.standard.string(forKey: "bearer_token") else { return }
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("Bearer " + bearer_token, forHTTPHeaderField: "Authorization")

        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            if let error = error {
                print("error: \(error)")
                self.removeSpinner()
                self.showError(error.localizedDescription)
            } else {
                if let response = response as? HTTPURLResponse {
                    print("getEstablishments statusCode: \(response.statusCode)")
                }
                if let data = data, let dataString = String(data: data, encoding: .utf8) {
                    print("getEstablishments data: \(dataString)")
                    do {
                        let dataObject = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.allowFragments) as Any
                        print("dataObject: \(dataObject)")
                        if let dataObject = dataObject as? [String: Any], let establishments = dataObject["establishments"] as? [Any] {
                            self.removeSpinner()
                            self.establishmentsList = establishments
                            self.showEstablishmentsList()
                        } else{
                            self.removeSpinner()
                            if let dataObject = dataObject as? [String: Any], let error = dataObject["message"] as? String {
                                self.showError(error)
                            }
                        }
                    } catch {
                        self.removeSpinner()
                        self.showError("parse error")
                    }
                } else {
                    self.removeSpinner()
                    self.showError("parse error")
                }
            }
        }
        task.resume()
    }

    func login() {
        showSpinner(onView: self.view)
        var url = URL(string: "https://cluster-dev.stay-app.com/int/guest/login")!
        if (prodEnvironment) {
            url = URL(string: "https://cluster.stay-app.com/guest/login")!
        }

        let pushToken = UserDefaults.standard.string(forKey: "push_token")
        
        var bodyData = [
            "deviceUid": deviceUidLabel.text!,
            "country": (countryLabel.text ?? "GB"),
            "os": osLabel.text!,
            "version": versionLabel.text!,
            "model": modelLabel.text!,
            "language": (!(languageTextField.text?.isEmpty ?? true) ? languageTextField.text! : "en"),
            "establishment": establishmentIdTextField.text!,
            "externalProviderId": Int(externalProviderIdLabel.text!)!,
            "application": applicationIdentifier
            ] as [String : Any]
        if let pushToken = pushToken, !pushToken.isEmpty {
            bodyData["pushToken"] = pushToken
        }
        if let locator = locatorTextField.text, !locator.isEmpty {
            bodyData["locator"] = locator
        }
        if let completeName = nameTextField.text, !completeName.isEmpty {
            bodyData["name"] = completeName
        }
        if let room = roomTextField.text, !room.isEmpty {
            bodyData["room"] = room
        }
        if let checkIn = checkInTextField.text, !checkIn.isEmpty {
            bodyData["checkIn"] = checkIn
        }
        if let checkOut = checkOutTextField.text, !checkOut.isEmpty {
            bodyData["checkOut"] = checkOut
        }
        if let eventType = eventTypeTextField.text, !eventType.isEmpty {
            bodyData["eventType"] = eventType
        }
        guard let bearer_token = UserDefaults.standard.string(forKey: "bearer_token") else { return }
        let client_bearer_token = UserDefaults.standard.string(forKey: "client_bearer_token")
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = try? JSONSerialization.data(withJSONObject:bodyData)
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("Bearer " + bearer_token, forHTTPHeaderField: "Authorization")
        if (client_bearer_token != nil) {
            request.addValue(client_bearer_token!, forHTTPHeaderField: "Stay-Authorization")
        }

        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            if let error = error {
                print("error: \(error)")
                self.removeSpinner()
                self.showError(error.localizedDescription)
            } else {
                if let response = response as? HTTPURLResponse {
                    print("login statusCode: \(response.statusCode)")
                }
                if let data = data, let dataString = String(data: data, encoding: .utf8) {
                    print("login data: \(dataString)")
                    do {
                        let dataObject = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.allowFragments) as Any
                        print("dataObject: \(dataObject)")
                        if let dataObject = dataObject as? [String: Any], let info = dataObject["data"] {
                            if let info = info as? [String: Any], let access_token = info["userKey"] {
                                UserDefaults.standard.set(access_token, forKey: "client_bearer_token")
                                self.sendForm()
                            } else{
                                self.removeSpinner()
                                if let error = dataObject["message"] as? String {
                                    self.showError(error)
                                }
                            }
                        } else{
                            self.removeSpinner()
                            if let dataObject = dataObject as? [String: Any], let error = dataObject["message"] as? String {
                                self.showError(error)
                            }
                        }
                    } catch {
                        self.removeSpinner()
                        self.showError("parse error")
                    }
                } else {
                    self.removeSpinner()
                    self.showError("parse error")
                }
            }
        }
        task.resume()
    }

    func sendForm() {
        showSpinner(onView: self.view)
        var url = URL(string: "https://cluster-dev.stay-app.com/int/catalogue/front/page")!
        if (prodEnvironment) {
            url = URL(string: "https://cluster.stay-app.com/catalogue/front/page")!
        }

        guard let bearer_token = UserDefaults.standard.string(forKey: "bearer_token") else { return }
        guard let client_bearer_token = UserDefaults.standard.string(forKey: "client_bearer_token") else { return }
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("Bearer " + bearer_token, forHTTPHeaderField: "Authorization")
        request.addValue(client_bearer_token, forHTTPHeaderField: "Stay-Authorization")
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            if let error = error {
                print("error: \(error)")
                self.removeSpinner()
                self.showError(error.localizedDescription)
            } else {
                if let response = response as? HTTPURLResponse {
                    print("frontpage statusCode: \(response.statusCode)")
                }
                if let data = data, let dataString = String(data: data, encoding: .utf8) {
                    print("frontpage data: \(dataString)")
                    do {
                        let dataObject = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.allowFragments) as Any
                        if let dataObject = dataObject as? [String: Any] {
                            DispatchQueue.main.async {
                                self.setupAdditionalData()
                                self.dataObject = dataObject
                                self.performSegue(withIdentifier: "ShowWebViewSegue", sender: nil)
                                self.removeSpinner()
                            }
                        } else {
                            self.removeSpinner()
                            if let dataObject = dataObject as? [String: Any], let error = dataObject["message"] as? String {
                                self.showError(error)
                            }
                        }
                    } catch {
                        self.removeSpinner()
                        self.showError("parse error")
                    }
                } else {
                    self.removeSpinner()
                    self.showError("parse error")
                }
            }
        }
        task.resume()
    }
    
    func showError(_ error: String) {
        let alert = UIAlertController(title: "Error", message: error, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Done", style: UIAlertAction.Style.default, handler: nil))
        DispatchQueue.main.async {
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func environmentSwitchChanged(_ sender: UISwitch) {
        prodEnvironment = sender.isOn
        UserDefaults.standard.removeObject(forKey: "client_bearer_token")
        setupAdditionalData()

    }

    @IBAction func sendButtonClicked(_ sender: Any) {
        if !validForm() {
            selectNextTextField()

            return
        }
        saveData()
        getAppBearer{
            self.login()
        }
    }

    @IBAction func resetButtonClicked(_ sender: Any) {
        resetData()
    }

    @IBAction func pickerViewDoneButtonClicked(_ sender: Any) {
        activeTextField.resignFirstResponder()
    }

    @IBAction func deleteTokenButtonClicked(_ sender: Any) {
        UserDefaults.standard.removeObject(forKey: "client_bearer_token")
        UserDefaults.standard.removeObject(forKey: "bearer_token")
        setupAdditionalData()
    }

    @IBAction func copyPushTokenButtonClicked(_ sender: Any) {
        UIPasteboard.general.string = pushTokenLabel.text
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowWebViewSegue" {
            if var urlString = dataObject["mainPage"] as? String {
                let webViewController = segue.destination as? WebViewViewController
#if DEBUG_LOCAL
                urlString = urlString.replacingOccurrences(of: "https://stay-pwa-qa-int.herokuapp.com", with: "http://192.168.0.10:8080")
                urlString = urlString.replacingOccurrences(of: "https://pwa.stay-app.com", with: "http://192.168.0.10:8080")
#endif
                urlString = urlString.replacingOccurrences(of: "https://pwa.stay-app.com", with: "https://pwa-prod.stay-app.com")

                webViewController?.urlString = urlString
            }
        }
    }

    @objc func textFieldDidChange(_ sender: Any) {
        sendButton.isEnabled = validForm()
    }

}

extension FormViewController: EstablishmentsListDelegate {
    func establishmentSelected(_ establishment: Dictionary<AnyHashable, Any>) {
        establishmentIdTextField.text = establishment["externalId"] as? String
        sendButton.isEnabled = validForm()
        self.dismiss(animated: true, completion: nil)
    }
}

extension FormViewController: UITextFieldDelegate {

    func textFieldDidBeginEditing(_ textField: UITextField) {
         activeTextField = textField
        
        if (activeTextField.text == "" && (activeTextField == checkInTextField || activeTextField == checkOutTextField)) {
            let datePicker = activeTextField == checkInTextField ? checkinDatePicker : checkoutDatePicker;
            handleDatePicker(sender: datePicker)
        }
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        selectNextTextField()
        
        return true
    }
}
extension URLRequest {
    public func cURL(pretty: Bool = false) -> String {
        let newLine = pretty ? "\\\n" : ""
        let method = (pretty ? "--request " : "-X ") + "\(self.httpMethod ?? "GET") \(newLine)"
        let url: String = (pretty ? "--url " : "") + "\'\(self.url?.absoluteString ?? "")\' \(newLine)"
        
        var cURL = "curl "
        var header = ""
        var data: String = ""
        
        if let httpHeaders = self.allHTTPHeaderFields, httpHeaders.keys.count > 0 {
            for (key,value) in httpHeaders {
                header += (pretty ? "--header " : "-H ") + "\'\(key): \(value)\' \(newLine)"
            }
        }
        
        if let bodyData = self.httpBody, let bodyString = String(data: bodyData, encoding: .utf8) {
            data = "--data '\(bodyString)'"
        }
        
        cURL += method + url + header + data
        
        return cURL
    }
}
extension FormViewController {
    func showSpinner(onView : UIView) {
        if (spinnerView != nil) {
            return
        }
        let spinnerView = UIView.init(frame: onView.bounds)
        spinnerView.backgroundColor = UIColor.init(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5)
        let ai = UIActivityIndicatorView.init(style: .whiteLarge)
        ai.startAnimating()
        ai.center = spinnerView.center
        
        DispatchQueue.main.async {
            spinnerView.addSubview(ai)
            onView.addSubview(spinnerView)
        }
        
        self.spinnerView = spinnerView
    }
    
    func removeSpinner() {
        DispatchQueue.main.async {
            self.spinnerView?.removeFromSuperview()
            self.spinnerView = nil
        }
    }
}
