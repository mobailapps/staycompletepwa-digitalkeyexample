# Stay Frontpage API Example App

## What
---------

Stay provides an API that let you create your own frontpage and redirect your users to the needed sections of Stay's PWA.

This is an example app that consumes this API, builds a simple frontpage and redirects to the PWA sections. It also handles all Stay notifications (direct alerts, updates in guest's requests, etc.).

## How
---------

### API ###

You can see the API documentation in: 

https://stayapp.atlassian.net/wiki/spaces/PUBLIC/pages/2223866088/API+REST


### Example App ###

The app has three main scenes:

* **FormViewController:** 
It is a simple form to fullfit the fields needed for the front request. The establishment is the only one required.

* **FrontViewController:** 
This viewController gets the front request response and draws all the elements of the frontPage. 

While the front is been painted, we instantiate a webview in the background and load the 'preLoadPage' attribute's url, in order to preload the PWA.

* **WebViewViewController:** 
When the user clicks on one element, we already have the PWA loaded in this viewController's WKWebView, so we execute PWA's 'redirectFromNative' method, that handles the redirections to Stay's PWA sections.

When the user goes back to the frontpage, we need to execute PWA's 'resetFromNative' method.
